// Groups

class Group {
    constructor() {
        this.members = [];
    }

    add(member) {
        this.members.push(member);
    }

    delete(member) {
        let position = this.members.indexOf(member);
        this.members.splice(position, 1); // remove 1 element at position
    }

    has(member) {
        return this.members.some(i => i === member);         
    }

    static from(iterable) {
        // create new group from iterable object
        let group = new Group();
        for (let i of iterable) {
            group.add(i);
        }
        return group;
    }
}

let g1 = new Group();
console.log(g1);
let o = { 'v':1 };
g1.add(o);
g1.add(2);
console.log(g1);
g1.delete(o);
console.log(g1);
console.log(g1.has(o));
console.log(g1.has(2));

console.log(Group.from([1,2,3]));
