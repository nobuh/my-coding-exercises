// Iterable Groups

class Group {
    constructor() {
        this.members = [];
    }

    add(member) {
        this.members.push(member);
    }

    delete(member) {
        let position = this.members.indexOf(member);
        this.members.splice(position, 1); // remove 1 element at position
    }

    has(member) {
        return this.members.some(i => i === member);         
    }

    static from(iterable) {
        // create new group from iterable object
        let group = new Group();
        for (let i of iterable) {
            group.add(i);
        }
        return group;
    }

    [Symbol.iterator]() {
        return new GroupIterator(this);
    }
}

class GroupIterator {
    constructor(group) {
        this.index = 0;
        this.group = group;
    }

    next() {
        if (this.index < this.group.members.length) {
            return {value: this.group.members[this.index++], done: false};
        } else {
            return {done: true};
        }
    }
}

const obj = { 'k': 'v' };
const group = Group.from([obj, 2,3]);
console.log(group);
for (const v of group) {
    console.log(v);
}
