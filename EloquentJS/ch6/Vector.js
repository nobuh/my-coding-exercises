// A Vector Type

class Vec {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    plus(vec) {
        let x = this.x + vec.x;
        let y = this.y + vec.y;
        return new Vec(x, y);
    }

    minus(vec) {
        let x = this.x - vec.x;
        let y = this.y - vec.y;
        return new Vec(x, y);
    }

    length() {
        return Math.sqrt(this.x**2 + this.y**2);
    }
    
}

v1 = new Vec(1,1);
v2 = new Vec(2,2);

v3 = v1.plus(v2);
console.log(`${v3.x}, ${v3.y}`);

v4 = v1.minus(v2);
console.log(`${v4.x}, ${v4.y}`);

console.log( v4.length() );
