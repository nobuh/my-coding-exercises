// Everything

function everyByLoop(array, test) {
    let result = true;
    for(let item of array) {
        result = result && test(item);
    }
    return result;
}

console.log( everyByLoop([2, 4, 6], n => (n % 2) == 0) );  
console.log( everyByLoop([2, 3, 6], n => (n % 2) == 0) );  

function everyBySome(array, test) {
    let testInverse = function (x) {
        return ! test(x);
    };
    return ! array.some(testInverse);
}

console.log( everyBySome([2, 4, 6], n => (n % 2) == 0) );  
console.log( everyBySome([2, 3, 6], n => (n % 2) == 0) );  
