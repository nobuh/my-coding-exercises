// Your Own Loop

function loop(value, test, update, body) {
    for( let i = value; test(i); i = update(i) ) {
        body(i);
    }
}

loop(1, i => i <= 10, i => i + 1, console.log); 
