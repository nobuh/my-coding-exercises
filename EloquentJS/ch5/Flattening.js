let arrays = [[1, 2, [3, 4, 5]], [6]];

function flattening(a) {
  return a.reduce( (i, v) => Array.isArray(v) ? i.concat(flattening(v)) : i.concat(v), [] );
}

console.log(flattening(arrays));
