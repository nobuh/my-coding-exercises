let size = 15;

for (let y = 1; y <= size; y++) {
    let line = "";
    for (let x = 1; x <= size; x++) {
        if ( (x % 2) + (y % 2) == 1 ) {
            // only x or y is odd. 
            line += "#";
        } else {
            line += " ";
        }
    }
    console.log(line);
}
