// Reverse array with copy
function reverseArray(before) {
    after = [];
    for (let i of before) {
       after.unshift(i);
    }
    return after;
}

// Reverse Array without copy. modify original array.
function reverseArrayInPlace(array) {
    for (let i = 0; i < array.length / 2; i++) {
        // swap head and tail
        let head = array[i];
        let tail = array[(array.length - 1) - i]; // tail index is length - 1 
        array[i] = tail;
        array[(array.length - 1) - i] = head;
    }
}  
