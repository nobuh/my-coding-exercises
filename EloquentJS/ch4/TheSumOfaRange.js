// The Sum of a Range
function range(start, end, step = 1) {
    let a = [];
    if (step > 0) {
        for (let i = start; i <= end; i += step) {
            a.push(i);
        }
    } else {
        for (let i = start; i >= end; i += step) {
            a.push(i);
        }
    } 
    return a;
}

function sum(numbers) {
    let n = 0;
    for (let i of numbers) {
         n += i;
    }
    return n;
} 
