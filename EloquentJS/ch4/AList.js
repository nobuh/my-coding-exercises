// A List

function arrayToList(array) {
    if (array.length > 1) {
        let restArray = [];
        for (let i = 1; i < array.length; i++) {
            restArray[i - 1] = array[i];
        } 
        return {
            value: array[0],
            rest: arrayToList(restArray)
        };
    } else {
        return {
            value: array[0],
            rest: null
        };
    }
}

function listToArray(list) {
    let array = [list.value];
    if (list.rest != null) {
        array2 = array.concat(listToArray(list.rest));
        return array2;
    } else {
        return array;
    }
}

function prepend(item, list) {
    return { value: item, rest: list };
}

function nth(list, index) {
    arr = listToArray(list);
    if (0 <= index && index < arr.length) {
        return arr[index];
    } else {
        return undefined;
    }
}
