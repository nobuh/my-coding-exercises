// BeanCounting

function countChar(str, ch) {
    let n = 0;
    if (ch.length == 1) {
        for (let i = 0; i < str.length; i++) {
            if (str[i] === ch) {
                n++;
            }
        }
    }
    return n;
}

function countBs(str) {
    return countChar(str, 'B');
}
