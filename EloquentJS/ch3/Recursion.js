// Recursion 
function isEven(n) {
    if (n == 0) {
        return "even";
    } else if (n * n == 1) {
        return "odd";
    } else if (n < 0) {
        return isEven(n + 2);
    } else {
        return isEven(n - 2);
    }
}
