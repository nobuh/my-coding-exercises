// exercise 1.2 
// print index and its value.
package main

import (
	"fmt"
	"os"
)

func main() {
	for i, arg := range os.Args[:] {
	    fmt.Print(i) 
        fmt.Println(" " + arg)
	}
}
